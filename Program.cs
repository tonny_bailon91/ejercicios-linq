﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ListaAleatorias
{
    class Program
    {
        public static void Main(string[] args)
        {


            static void Main (string[] args, List<ClaseClientes> listaClaseClientes, List<ClaseFactura> listaClaseFactura)
            {

                Console.Clear();
                Console.Write(
                    "Hello World!" +
                    "consulta de numero aleatorios");

                Console.WriteLine("Pulse enter para asi continuar...");
                Console.ReadKey();
                //Lista de 20 números aleatorios
                int[] numero = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 };
                listaClaseClientes = new List<ClaseClientes>
                        {
                        new ClaseClientes {Nombre= "Xande", IdCliente= 1122 },
                        new ClaseClientes {Nombre= "Mateo", IdCliente= 1143 },
                        new ClaseClientes {Nombre= "Yeralis", IdCliente= 1165 },
                        new ClaseClientes {Nombre= "Antonia", IdCliente= 1178 },
                        new ClaseClientes {Nombre= "Joel", IdCliente= 1189 },
                        new ClaseClientes {Nombre= "Jefferson", IdCliente= 1198 },
                        new ClaseClientes {Nombre= "Gabriel", IdCliente= 1187 },
                        new ClaseClientes {Nombre= "Rom", IdCliente= 1165 },
                        new ClaseClientes {Nombre= "Bruno", IdCliente= 1154 },
                        new ClaseClientes {Nombre= "Dalia", IdCliente= 1143 },
                        };
                listaClaseFactura = new List<ClaseFactura>
                        {
                        new ClaseFactura {Detalles= "deporte", IdCliente= 1122, Fecha=new DateTime (2010,01, 01),  Total=1000, Numero= 10 },
                        new ClaseFactura {Detalles= "atleta", IdCliente= 1143, Fecha=new DateTime (2010, 31, 12), Total=2000, Numero= 9 },
                        new ClaseFactura {Detalles= "baila", IdCliente= 1165, Fecha=new DateTime (2006, 08, 11), Total=3000, Numero= 8 },
                        new ClaseFactura {Detalles= "cantante", IdCliente= 1178,  Fecha=new DateTime (2012, 10, 11), Total=4000, Numero= 7 },
                        new ClaseFactura {Detalles= "jugador", IdCliente= 1189, Fecha=new DateTime (2015, 03, 09), Total=5000, Numero= 6 },
                        new ClaseFactura {Detalles= "dueño", IdCliente= 1198, Fecha=new DateTime (2016, 03, 11), Total=6000, Numero= 5 },
                        new ClaseFactura {Detalles= "preso", IdCliente= 1187, Fecha=new DateTime (2021, 05, 04), Total=7000, Numero= 4 },
                        new ClaseFactura {Detalles= "estudiante", IdCliente= 1165, Fecha=new DateTime (1991, 06, 10), Total=8000, Numero= 3 },
                        new ClaseFactura {Detalles= "xxx", IdCliente= 1154,  Fecha=new DateTime (2011, 07, 03), Total=9000, Numero= 2},
                        new ClaseFactura {Detalles= "ladron", IdCliente= 1143, Fecha=new DateTime (2012, 08, 01), Total=10000, Numero= 1 },
                        };
                int opcion = 0;
                do
                {
                    Console.WriteLine("¿Indique que desea relizar?");
                    Console.WriteLine("\n" +
                        "\n 1.-  Mostrar en consola todos los números primos." +
                        "\n 2.-  Mostrar en consola la suma de todos los elementos." +
                        "\n 3.-  Generar una nueva lista con el cuadrado de los números." +
                        "\n 4.-  Generar una nueva lista con los números primos." +
                        "\n 5.-  Obtener el promedio de todos los números mayores a 50." +
                        "\n 6.-  Contar la cantidad de números pares e impares." +
                        "\n 7.-  Mostrar en consola, el número y la cantidad de veces que este se encuentra en la lista. " +
                        "\n 8.-  Mostrar en consola los elementos de forma descendente." +
                        "\n 9.-  Mostrar en consola los números únicos." +
                        "\n 10.- Sumar todos los números únicos de la lista." +
                        "\n 11.- Pasar al proceso de búsqueda de cliente y factura" +
                        "\n 12.- Salir del progrma\n");
                    Console.WriteLine("Marque una Opción :");
                    opcion = Convert.ToInt32(Console.ReadLine());
                    switch (opcion)
                    {
                        case 1:
                            Console.Clear();
                            var PrimosQuery = from primos in numero
                                              where primos % 2 != 0
                                              select primos;
                            foreach (var Num in PrimosQuery)
                            {
                                Console.WriteLine("el número primo es: " + Num);
                            }
                            break;
                        case 2:
                            Console.Clear();
                            IEnumerable<int> sumaQuery = from numeros in numero
                                                         select numeros;
                            Console.WriteLine("Todos la suma de los números da:  " + sumaQuery.Sum());
                            break;
                        case 3:
                            Console.Clear();
                            var nuevalista = from nueva in numero
                                             where nueva > 0
                                             select nueva;
                            var nuevoforma = (from nueva in numero
                                              where nueva > 0
                                              select nueva).ToArray();

                            Console.Write("Los números son: ");

                            foreach (var cuadrado in nuevalista)
                            {
                                Console.Write(cuadrado + ",");
                            }
                            Console.Write("\n");
                            Console.Write("El cuadrado del número es: ");

                            foreach (var cuadrado in nuevoforma)
                            {
                                Console.Write(Math.Pow(cuadrado, 2) + ",");
                            }
                            Console.WriteLine("\n");
                            break;
                        case 4:
                            Console.Clear();
                            var NumerosNPrimos = (from NuevosPrimos in numero
                                                  where NuevosPrimos % 2 != 0
                                                  select NuevosPrimos).ToArray();
                            foreach (var NuevosNumeros in NumerosNPrimos)
                            {
                                Console.Write(NuevosNumeros + ",");
                            }
                            Console.Write("\n");
                            break;
                        case 5:
                            Console.Clear();
                            IEnumerable<int> NumeroMayor50 = from Numeromayor in numero
                                                             where Numeromayor > 50
                                                             select Numeromayor;
                            foreach (var Nmayor in NumeroMayor50)
                            {
                                Console.Write("Numeros mayores a 50 son: ");
                                Console.Write(Nmayor + ",");
                            }
                            Console.WriteLine("\n");
                            Console.Write("los numeros mayores a 50 son y el promedio es  :" + NumeroMayor50.Average());
                            Console.WriteLine("\n");
                            break;
                        case 6:
                            Console.Clear();
                            var NumerosPar = from contanumeros in numero
                                             group contanumeros by (contanumeros % 2) == 0 into Total
                                             select Total;
                            foreach (var Numeropar in NumerosPar)
                            {
                                if (Numeropar.Key)
                                {
                                    Console.WriteLine("La cantidad de números pares es de : " + NumerosPar.Count());
                                }
                                if (!Numeropar.Key)
                                {
                                    Console.WriteLine("La cantidad de números impares es de : " + NumerosPar.Count());
                                }
                            }
                            Console.WriteLine("\n");
                            break;
                        case 7:
                            Console.Clear();
                            var numerosrepetidos =
                                from Numerosrepetidos in numero
                                group Numerosrepetidos by Numerosrepetidos into grupoRepetidos
                                select grupoRepetidos;
                            foreach (var item in numerosrepetidos)
                            {
                                Console.WriteLine("El número es:  " + item.Key + " Y este se repite:   " + item.Count() + " num de vez/veces.");
                            }
                            Console.WriteLine("\n");
                            break;
                        case 8:
                            Console.Clear();
                            var DescendentesNumeros =
                                from numerodescendentes in numero
                                orderby numerodescendentes descending
                                select numerodescendentes;

                            Console.Write("En orden descendentes los números quedan en el siguiente orden: ");

                            foreach (var item in DescendentesNumeros)
                            {
                                Console.Write(+item + ",");
                            }
                            Console.WriteLine("\n");
                            break;
                        case 9:
                            Console.Clear();
                            var Numerosunicos =
                                from Numunico in numero
                                group Numunico by Numunico into Numunicotot
                                where Numunicotot.Count() == 1
                                select Numunicotot;

                            Console.Write("Estos son los números únicos: ");

                            foreach (var Num in Numerosunicos)
                            {
                                Console.Write(Num.Key + ",");
                            }
                            Console.WriteLine("\n");
                            break;
                        case 10:
                            Console.Clear();
                            int SumaUnicos = 0;
                            var numerosTotalUnicos =
                                from numerosunico in numero
                                group numerosunico by numerosunico into sumaGroup
                                where sumaGroup.Count() == 1
                                select sumaGroup;
                            foreach (var suma in numerosTotalUnicos)
                            {
                                SumaUnicos += suma.Key;
                            }
                            Console.WriteLine("la suma total de numeros unicos son: " + SumaUnicos);
                            Console.WriteLine("\n");
                            break;
                        case 11:
                            Console.Write(
                              "La consulta de cliente - factura ");
                            Console.WriteLine("Presione enter para asi continuar...");
                            Console.ReadKey();
                            Console.WriteLine("Mostrar en consola los 3 clientes con más monto en ventas");
                            var Tresmontoventa =
                                (from ClaseClientes in listaClaseClientes
                                 join ClaseFactura in listaClaseFactura
                                 on ClaseClientes.IdCliente equals ClaseFactura.IdCliente
                                 orderby ClaseFactura.Total descending

                                 select new { ClaseClientes.Nombre, ClaseFactura.Total }).Take(3);
                            Console.WriteLine("Los tres mayores montos de venta son :");
                            foreach (var item in Tresmontoventa)
                            {
                                {
                                    Console.WriteLine("El nombre del cliente es: " + item.Nombre + "  con un total de : " + item.Total);
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola los 3 clientes con menos monto en ventas.");
                            var Tresmontoventamenor = (from ClaseClientes in listaClaseClientes
                                                       join Clasefactura in listaClaseFactura
                                                       on ClaseClientes.IdCliente equals Clasefactura.IdCliente
                                                       orderby Clasefactura.Total ascending
                                                       select new { ClaseClientes.Nombre, Clasefactura.Total }).Take(3);
                            Console.WriteLine("Los tres menores montos de venta son :");
                            foreach (var item in Tresmontoventamenor)
                            {
                                {
                                    Console.WriteLine("El nombre del cliente es : " + item.Nombre + " con un total de : " + item.Total);
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola el cliente con más ventas realizadas");
                            var Clientemasventa = (from ClaseClientes in listaClaseClientes
                                                   join ClaseFactura in listaClaseFactura
                                                   on ClaseClientes.IdCliente equals ClaseFactura.IdCliente
                                                   where ClaseClientes.IdCliente == ClaseFactura.IdCliente
                                                   select new { ClaseClientes.IdCliente, ClaseClientes.Nombre, ClaseFactura.Numero }).Take(2);
                            foreach (var item in Clientemasventa)
                            {
                                {
                                    Console.WriteLine("El cliente con más venta es : " + item.Nombre + " con número de venta : " + item.Numero);
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola el cliente y la cantidad de ventas realizadas");
                            var ventasrealizadas = from ventas in listaClaseFactura
                                                   join ClaseClientes in listaClaseClientes
                                                   on ventas.IdCliente equals ClaseClientes.IdCliente
                                                   where ventas.Fecha < new DateTime(2010, 01, 01)
                                                   select new { ClaseClientes.Nombre, ventas.Fecha };
                            foreach (var item in ventasrealizadas)
                            {
                                {
                                    Console.WriteLine("Ventas del cliente : " + item.Nombre + "  con dos ventas realizadas ");
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola las ventas realizadas hace menos de 1 año.");
                            var Ventashaceunaño = from ventas in listaClaseFactura
                                                  join ClaseClientes in listaClaseClientes
                                                  on ventas.IdCliente equals ClaseClientes.IdCliente
                                                  where ventas.Fecha > new DateTime(2021, 05, 04)
                                                  select new { ClaseClientes.Nombre, ventas.Fecha };
                            foreach (var item in Ventashaceunaño)
                            {
                                {
                                    Console.WriteLine("Venta del cliente : " + item.Nombre + "  con fecha del  : " + item.Fecha);
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola las ventas más antiguas");
                            var Ventamasantigua = from ventas in listaClaseFactura
                                                  join ClaseClientes in listaClaseClientes
                                                  on ventas.IdCliente equals ClaseClientes.IdCliente
                                                  where ventas.Fecha < new DateTime(2016, 03, 11)
                                                  select new { ClaseClientes.Nombre, ventas.Fecha };
                            foreach (var item in Ventamasantigua)
                            {
                                {
                                    Console.WriteLine("La venta más antigua es de : " + item.Nombre + " con fecha de : " + item.Fecha);
                                }
                            }
                            Console.WriteLine("\n");
                            Console.WriteLine("Mostrar en consola los clientes que tengan una venta cuya observación comience con Prob");
                            var observaciones = listaClaseFactura.Where(ListaFactura => ListaFactura.Detalles.Contains("Prob", StringComparison.CurrentCultureIgnoreCase));
                            foreach (var item in observaciones)
                            {
                                {
                                    Console.WriteLine(item.Detalles + " ID del cliente :" + item.IdCliente);
                                }
                            }
                            break;
                        case 12:
                            Console.WriteLine("Fin. Ya no existen más opciones de búsqueda o muestra de resultados");
                            break;
                        default:
                            break;
                    }
                } while (opcion != 12);
                Console.ReadKey();
            }
        }
    }
}
