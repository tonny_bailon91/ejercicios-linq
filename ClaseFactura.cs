﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaAleatorias
{
    class ClaseFactura
    {
        
        public int IdCliente { get; set; }
        public string Detalles { get; set; }
        public int Numero { get; set; }
        public decimal Total { get; set; }
        public DateTime Fecha { get; set; }

    }
}
